﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace El_Banco
{
    public class Clase_Ejecutiva
    {
        private float abonarACuenta;

        private float retirarDeCuenta;

        private float calcularInterés;

        private float credito;

        public float AbonarACuenta { get => abonarACuenta; set => abonarACuenta = value; }
        public float RetirarDeCuenta { get => retirarDeCuenta; set => retirarDeCuenta = value; }
        public float CalcularInterés { get => calcularInterés; set => calcularInterés = value; }
        public float Credito { get => credito; set => credito = value; }
    }
}

