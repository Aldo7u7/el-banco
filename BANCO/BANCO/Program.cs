﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace El_Banco
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" ***Bienvenido a BANAMEX*** ");
            Console.WriteLine(" El Banco Fuerte de Mexico");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            Console.WriteLine(" ``Cuenta Regular`` ");
            Cuenta_Regular cuenta;
            cuenta = new Cuenta_Regular();
            cuenta.Saldo_regular = 200;
            cuenta.AbonarACuenta = 500;
            cuenta.RetirarDeCuenta = 75;
            cuenta.CalcularInterés = 1;
            cuenta.Credito = 500;
            float saldoneto, saldototal;
            saldoneto = cuenta.Saldo_regular + cuenta.AbonarACuenta;
            saldototal = saldoneto - cuenta.RetirarDeCuenta;
            Console.WriteLine(" El Saldo Actual es:" + cuenta.Saldo_regular + " pesos");
            Console.WriteLine(" El Abono a su cuenta es:" + cuenta.AbonarACuenta + " pesos");
            Console.WriteLine(" Su Saldo Neto es:" + saldoneto + " pesos");
            Console.WriteLine(" Usted Retiro:" + cuenta.RetirarDeCuenta + " pesos");
            Console.WriteLine(" Su saldo Total es:" + saldototal);
            Console.WriteLine(" Su credito pedido fue de:" + cuenta.Credito + " pesos");
            Console.WriteLine(" Su Interes por dia de retraso sera del: " + cuenta.CalcularInterés + "%" + " o 5 pesos");

            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine(" ``Cuenta Dorada`` ");
            Cuenta_Dorada cuentadora;
            cuentadora = new Cuenta_Dorada();
            cuentadora.Saldo_dorado = 200;
            cuentadora.AbonarACuenta = 500;
            cuentadora.RetirarDeCuenta = 75;
            cuentadora.CalcularInterés = 10;
            cuentadora.Credito = 500;
            float saldoneto1, saldototal1;
            saldoneto1 = cuenta.Saldo_regular + cuenta.AbonarACuenta;
            saldototal1 = saldoneto1 - cuenta.RetirarDeCuenta;
            Console.WriteLine(" El Saldo Actual es:" + cuenta.Saldo_regular + " pesos");
            Console.WriteLine(" El Abono a su cuenta es:" + cuenta.AbonarACuenta + " pesos");
            Console.WriteLine(" Su Saldo Neto es:" + saldoneto1 + " pesos");
            Console.WriteLine(" Usted Retiro:" + cuenta.RetirarDeCuenta + " pesos");
            Console.WriteLine(" Su saldo Total es:" + saldototal1);
            Console.WriteLine(" Su credito pedido fue de:" + cuenta.Credito + " pesos");
            Console.WriteLine(" Su Interes por dia de retraso sera del: " + "10%" + " o 50 pesos");
            Console.ReadKey();


        }
    }
}